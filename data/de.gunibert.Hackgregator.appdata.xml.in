<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2019-2020 Günther Wagner -->
<component type="desktop-application">
  <id>de.gunibert.Hackgregator</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0</project_license>
  <name>Hackgregator</name>
  <summary>Reading Hacker News</summary>
  <description>
    <p>
      Hackgregator is a tool for reading Hacker News on your desktop or mobile phone.
    </p>
  </description>
  <screenshots>
    <screenshot type="default">
      <image height="661" width="967">https://gitlab.com/gunibert/hackgregator/-/raw/master/data/screenshots/hackgregator-landing.png</image>
      <caption>Landing screen</caption>
    </screenshot>
    <screenshot>
      <image height="661" width="967">https://gitlab.com/gunibert/hackgregator/-/raw/master/data/screenshots/hackgregator-article.png</image>
      <caption>Article screen</caption>
    </screenshot>
    <screenshot>
      <image height="661" width="967">https://gitlab.com/gunibert/hackgregator/-/raw/master/data/screenshots/hackgregator-comments.png</image>
      <caption>Comments screen</caption>
    </screenshot>
    <screenshot>
      <image height="661" width="624">https://gitlab.com/gunibert/hackgregator/-/raw/master/data/screenshots/hackgregator-mobile.png</image>
      <caption>Small screen</caption>
    </screenshot>
  </screenshots>
  <releases>
    <release version="0.2.0" date="2020-06-21">
      <description>
        <p>Hackgregator 0.2.0 contains various bug fixes and UI changes.</p>
        <ul>
          <li>Restructured the landing screen. Inspired by the website interaction.</li>
          <li>New App Icon!</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" date="2019-10-19"/>
  </releases>
  <provides>
    <id>de.gunibert.Hackgregator</id>
  </provides>
  <launchable type="desktop-id">de.gunibert.Hackgregator.desktop</launchable>
  <url type="homepage">https://gitlab.com/gunibert/hackgregator</url>
  <url type="bugtracker">https://gitlab.com/gunibert/hackgregator/issues</url>
  <update_contact>info@gunibert.de</update_contact>
  <content_rating type="oars-1.1" />
  <developer_name>Günther Wagner</developer_name>
</component>
