/* hn-comment-page.c
 *
 * Copyright 2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "hn-comment-page.h"
#include "hn-comment-row.h"
#include "hn-utils.h"
#include "hn-debug.h"

struct _HnCommentPage
{
  GtkBin parent_instance;

  GtkWidget *comment_list_box;
  GtkWidget *itemtext;

  GListModel *model;
};

G_DEFINE_TYPE (HnCommentPage, hn_comment_page, GTK_TYPE_BIN)

enum {
  SHOW_FULL_COMMENTS,
  N_SIGNALS
};

static guint signals[N_SIGNALS];

static void
on_comment_row_activated (GtkListBox    *box,
                          GtkListBoxRow *row,
                          gpointer       user_data)
{
  HnCommentPage *self = (HnCommentPage *)user_data;
  HnItem *item = NULL;
  gint idx;

  g_assert (HN_IS_COMMENT_PAGE (self));
  g_assert (GTK_IS_LIST_BOX (box));
  g_assert (HN_IS_COMMENT_ROW (row));

  idx = gtk_list_box_row_get_index (row);
  item = g_list_model_get_item (G_LIST_MODEL (self->model), idx);
  g_signal_emit (self, signals[SHOW_FULL_COMMENTS], 0, item, row);
}

HnCommentPage *
hn_comment_page_new (void)
{
  return g_object_new (HN_TYPE_COMMENT_PAGE, NULL);
}

static void
hn_comment_page_finalize (GObject *object)
{
  HnCommentPage *self = (HnCommentPage *)object;

  g_clear_object (&self->model);

  G_OBJECT_CLASS (hn_comment_page_parent_class)->finalize (object);
}

static void
hn_comment_page_class_init (HnCommentPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = hn_comment_page_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/Hackgregator/hn-comment-page.ui");
  gtk_widget_class_bind_template_child (widget_class, HnCommentPage, comment_list_box);
  gtk_widget_class_bind_template_child (widget_class, HnCommentPage, itemtext);
  gtk_widget_class_bind_template_callback (widget_class, on_comment_row_activated);

  signals [SHOW_FULL_COMMENTS] =
    g_signal_new ("show-full-comments",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  2,
                  HN_TYPE_ITEM,
                  HN_TYPE_COMMENT_ROW);
}

static void
hn_comment_page_init (HnCommentPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static GtkWidget *
hn_comment_page_create_comment_row (gpointer item,
                                    gpointer user_data)
{
  GtkWidget *comment_row = hn_comment_row_new (item);

  return comment_row;
}

void
hn_comment_page_bind_model (HnCommentPage *self,
                            GListModel    *model)
{
  g_return_if_fail (HN_IS_COMMENT_PAGE (self));
  g_return_if_fail (G_IS_LIST_MODEL (model));

  self->model = g_object_ref (model);
  gtk_list_box_bind_model (GTK_LIST_BOX (self->comment_list_box),
                           model,
                           hn_comment_page_create_comment_row,
                           self,
                           NULL);
}

void
hn_comment_page_load_text (HnCommentPage *self,
                           HnItem        *item)
{
  g_return_if_fail (HN_IS_COMMENT_PAGE (self));
  g_return_if_fail (HN_IS_ITEM (item));

  HN_ENTRY;

  gtk_widget_show (self->itemtext);

  if (hn_item_get_text (item) == NULL)
    gtk_label_set_text (GTK_LABEL (self->itemtext), hn_item_get_title (item));
  else
    gtk_label_set_markup (GTK_LABEL (self->itemtext), html_2_pango (hn_item_get_text (item)));

  HN_EXIT;
}

void
hn_comment_page_load_comments (HnCommentPage *self)
{
  g_return_if_fail (HN_IS_COMMENT_PAGE (self));

  gtk_widget_hide (self->itemtext);
}
