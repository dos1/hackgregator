/* hn-debug.h
 *
 * Copyright 2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "glib.h"

G_BEGIN_DECLS

/**
 * HN_LOG_LEVEL_TRACE: (skip)
 */
# define HN_LOG_LEVEL_TRACE ((GLogLevelFlags)(1 << G_LOG_LEVEL_USER_SHIFT))

#define HN_ENTRY \
  g_log(G_LOG_DOMAIN, HN_LOG_LEVEL_TRACE, "ENTRY: %s():%d", G_STRFUNC, __LINE__)
#define HN_EXIT \
  g_log(G_LOG_DOMAIN, HN_LOG_LEVEL_TRACE, " EXIT: %s():%d", G_STRFUNC, __LINE__)
#define HN_RETURN(_r) \
  G_STMT_START { \
    g_log(G_LOG_DOMAIN, HN_LOG_LEVEL_TRACE, " EXIT: %s():%d ", G_STRFUNC, __LINE__); \
    return _r; \
  } G_STMT_END


G_END_DECLS
