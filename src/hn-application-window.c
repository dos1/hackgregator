/* hn-application-window.c
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "hn-application-window"

#include "hn-application-window.h"
#include "hn-article-page.h"
#include "hn-comment-row.h"
#include "hn-story-list-box.h"
#include "hn-comment-page.h"
#include "hackernews-glib.h"
#include "hn-debug.h"
#include "hn-utils.h"
#define HANDY_USE_UNSTABLE_API
#include <handy.h>

struct _HnApplicationWindow
{
  GtkApplicationWindow parent_instance;

  /* ui */
  GtkWidget *appmenu;
  GtkWidget *headerbar;
  GtkWidget *deck;
  GtkWidget *back_btn;
  GtkWidget *refresh_btn;
  GtkWidget *direct_link_btn;
  GtkWidget *stack;
  GtkWidget *story_list_box;
  GtkWidget *comment_page;
  GtkWidget *loading_spinner;
  GtkWidget *article_page;
  GtkWidget *sections;

  /* business */
  HnClient *client;
  GListStore *item_list;
  GListStore *comment_list;
  gchar *active_section;
  GSettings *settings;
  GCancellable *cancellable;

  /* window state */
  gint current_width;
  gint current_height;
};

G_DEFINE_TYPE (HnApplicationWindow, hn_application_window, GTK_TYPE_APPLICATION_WINDOW)

static void
hn_application_window_show (GtkWidget *widget)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (widget);

  gint width, height;
  g_settings_get (self->settings, "window-size", "(ii)", &width, &height, NULL);

  if (width >= 0 && height >= 0)
    {
      gtk_window_resize (GTK_WINDOW (self), width, height);
    }

  GTK_WIDGET_CLASS (hn_application_window_parent_class)->show (widget);
}

static void
hn_application_window_size_allocate (GtkWidget     *widget,
                                     GtkAllocation *allocation)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (widget);

  GTK_WIDGET_CLASS (hn_application_window_parent_class)->size_allocate (widget, allocation);

  gtk_window_get_size (GTK_WINDOW (self), &self->current_width, &self->current_height);
}

static void
hn_application_window_destroy (GtkWidget *widget)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (widget);

  // save window size
  g_settings_set (self->settings, "window-size", "(ii)", self->current_width, self->current_height);

  GTK_WIDGET_CLASS (hn_application_window_parent_class)->destroy (widget);
}

static void
hn_application_window_setup_initial (HnApplicationWindow *self)
{
  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));

  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 5);
  gtk_widget_set_valign (box, GTK_ALIGN_CENTER);

  GtkWidget *image = gtk_image_new ();
  gtk_image_set_from_resource (GTK_IMAGE (image), "/de/gunibert/Hackgregator/news-icon.svg");
  gtk_box_pack_start (GTK_BOX (box), image, TRUE, TRUE, 0);

  GtkWidget *lbl = gtk_label_new ("");
  gtk_label_set_markup (GTK_LABEL (lbl), "<span font-weight=\"bold\" font-size=\"x-large\">No storie selected</span>");
  gtk_box_pack_start (GTK_BOX (box), lbl, TRUE, TRUE, 0);

  GtkWidget *sublbl = gtk_label_new ("Select a story from the sidebar to read news.");
  gtk_box_pack_start (GTK_BOX (box), sublbl, TRUE, TRUE, 0);

  /* gtk_list_box_set_placeholder (GTK_LIST_BOX (self->comment_list_box), box); */

  gtk_widget_show_all (box);

  HN_EXIT;
}

static void
hn_application_window_finalize (GObject *object)
{
  HnApplicationWindow *self = HN_APPLICATION_WINDOW (object);

  g_clear_pointer (&self->client, g_object_unref);
  g_list_store_remove_all (self->item_list);
  g_list_store_remove_all (self->comment_list);

  G_OBJECT_CLASS (hn_application_window_parent_class)->finalize (object);
}

static void
hn_application_window_fetched_comments (GObject      *source_object,
                                        GAsyncResult *res,
                                        gpointer      user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;
  HnClient *client = (HnClient *) source_object;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_CLIENT (client));

  //TODO: error handling
  GPtrArray *comments = hn_client_get_comments_finish (client, res, NULL);

  for (guint i = 0; i < comments->len; i++) {
    HnItem *item = g_ptr_array_index (comments, i);
    if (hn_item_get_deleted (item)) continue;

    g_list_store_append (self->comment_list, g_steal_pointer (&item));
  }

  gtk_widget_hide (self->loading_spinner);

  HN_EXIT;
}

static void
hn_application_window_fetched_items (GObject      *source_object,
                                     GAsyncResult *res,
                                     gpointer      user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;
  HnClient *client = (HnClient *) source_object;
  GError *error = NULL;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_CLIENT (client));

  GPtrArray *stories = hn_client_get_stories_finish (client, res, &error);
  if (error != NULL) {
    // TODO: handle error
    HN_EXIT;
    return;
  }

  gtk_widget_hide (self->loading_spinner);

  for (guint i = 0; i < stories->len; i++) {
    HnItem *item = g_ptr_array_index (stories, i);

    g_list_store_append (self->item_list, g_steal_pointer (&item));
  }

  g_ptr_array_unref (stories);

  HN_EXIT;
}

static void
_load_item_url (HnApplicationWindow *self,
                HnItem              *item)
{
  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_ITEM (item));

  hdy_deck_set_visible_child_name (HDY_DECK (self->deck), "webview_page");

  hn_article_page_set_active_item (HN_ARTICLE_PAGE (self->article_page), item);

  gtk_widget_hide (self->sections);
  gtk_widget_hide (self->refresh_btn);
  gtk_widget_hide (self->appmenu);

  gtk_widget_show (self->back_btn);
  gtk_widget_show (self->direct_link_btn);

  HN_EXIT;
}

static void
_load_item_text (HnApplicationWindow *self,
                 HnItem              *item)
{
  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_ITEM (item));

  hn_client_get_comments_async (self->client, item, NULL, hn_application_window_fetched_comments, self);

  hdy_deck_set_visible_child_name (HDY_DECK (self->deck), "comment_page");

  gtk_widget_hide (self->sections);
  gtk_widget_show (self->back_btn);
  gtk_widget_hide (self->refresh_btn);
  gtk_widget_show (self->loading_spinner);

  HN_EXIT;
}

static void
hn_application_window_show_story (HnStoryListBox *story_list_box,
                                  HnItem         *item,
                                  gpointer        user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_STORY_LIST_BOX (story_list_box));
  g_assert (HN_IS_ITEM (item));

  if (hn_item_get_url (item) == NULL)
    {
      g_list_store_remove_all (self->comment_list);
      hn_comment_page_load_text (HN_COMMENT_PAGE (self->comment_page), item);
      _load_item_text (self, item);
    }
  else
    {
      _load_item_url (self, item);
    }

  HN_EXIT;
}

static void
_load_comments_page (HnStoryListBox *story_list_box,
                     HnItem         *item,
                     gpointer        user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_STORY_LIST_BOX (story_list_box));
  g_assert (HN_IS_ITEM (item));

  g_list_store_remove_all (self->comment_list);

  if (g_strcmp0 (hdy_deck_get_visible_child_name (HDY_DECK (self->deck)), "webview_page") == 0) {
    item = hn_article_page_get_active_item (HN_ARTICLE_PAGE (self->article_page));
  }

  if (hn_item_get_url (item) == NULL)
    {
      hn_comment_page_load_text (HN_COMMENT_PAGE (self->comment_page), item);
    }
  else
    {
      hn_comment_page_load_comments (HN_COMMENT_PAGE (self->comment_page));
    }

  hn_client_get_comments_async (self->client, item, NULL, hn_application_window_fetched_comments, self);

  hdy_deck_set_visible_child_name (HDY_DECK (self->deck), "comment_page");
  gtk_widget_hide (self->sections);
  gtk_widget_hide (self->refresh_btn);
  gtk_widget_hide (self->appmenu);

  gtk_widget_show (self->back_btn);
  gtk_widget_show (self->loading_spinner);

  hn_article_page_set_active_item (HN_ARTICLE_PAGE (self->article_page), item);

  HN_EXIT;
}

static void
on_refresh_clicked_cb (GtkButton *btn,
                       gpointer   user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));

  g_list_store_remove_all (self->item_list);
  gtk_widget_show (self->loading_spinner);

  hn_client_get_stories_async (self->client, -1, self->active_section, NULL, hn_application_window_fetched_items, self);

  HN_EXIT;
}

static void
back_btn_clicked_cb (GtkButton *btn,
                     gpointer   user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));

  if (self->cancellable != NULL)
    {
      g_cancellable_cancel (self->cancellable);
      self->cancellable = NULL;
    }
  hn_article_page_load_uri (HN_ARTICLE_PAGE (self->article_page), "about:blank");
  g_list_store_remove_all (self->comment_list);

  hdy_deck_set_visible_child_name (HDY_DECK (self->deck), "item_page");

  gtk_widget_hide (self->back_btn);
  gtk_widget_hide (self->direct_link_btn);

  gtk_widget_show (self->sections);
  gtk_widget_show (self->refresh_btn);
  gtk_widget_show (self->appmenu);

  HN_EXIT;
}

static void
hn_application_window_webview_load_changed (WebKitWebView   *web_view,
                                            WebKitLoadEvent  load_event,
                                            gpointer         user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (WEBKIT_IS_WEB_VIEW (web_view));

  switch (load_event)
    {
    case WEBKIT_LOAD_STARTED:
      gtk_widget_show (self->loading_spinner);
      break;
    case WEBKIT_LOAD_FINISHED:
      gtk_widget_hide (self->loading_spinner);
      break;
    default:
      /* do nothing */
      break;
    }

  HN_EXIT;
}

static void
_direct_link_clicked_cb (GtkButton           *btn,
                         HnApplicationWindow *self)
{
  HN_ENTRY;

  g_return_if_fail (HN_IS_APPLICATION_WINDOW (self));

  // get item
  HnItem *active_item = hn_article_page_get_active_item (HN_ARTICLE_PAGE (self->article_page));
  gchar *url = hn_item_get_url (active_item);

  g_app_info_launch_default_for_uri (url, NULL, NULL);

  HN_EXIT;
}

static void
hn_application_window_section_changed (GtkComboBox *widget,
                                       gpointer     user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *) user_data;
  const gchar *endpoint_id = NULL;

  HN_ENTRY;

  g_assert (HN_IS_APPLICATION_WINDOW (self));

  g_list_store_remove_all (self->item_list);
  endpoint_id = gtk_combo_box_get_active_id (widget);
  hn_client_get_stories_async (self->client,
                               30,
                               endpoint_id,
                               NULL,
                               hn_application_window_fetched_items,
                               self);
  gtk_widget_show (self->loading_spinner);

  HN_EXIT;
}

static void
hn_application_window_class_init (HnApplicationWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = hn_application_window_finalize;
  widget_class->show = hn_application_window_show;
  widget_class->size_allocate = hn_application_window_size_allocate;
  widget_class->destroy = hn_application_window_destroy;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/Hackgregator/hn-application-window.ui");
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, appmenu);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, back_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, deck);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, refresh_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, direct_link_btn);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, story_list_box);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, comment_page);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, loading_spinner);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, article_page);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, headerbar);
  gtk_widget_class_bind_template_child (widget_class, HnApplicationWindow, sections);
  gtk_widget_class_bind_template_callback (widget_class, back_btn_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_refresh_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, _load_comments_page);
  gtk_widget_class_bind_template_callback (widget_class, _direct_link_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, hn_application_window_section_changed);
}

static void
_show_comments_full_fetched(GObject      *object,
                            GAsyncResult *res,
                            gpointer      user_data)
{
  HnCommentRow *row = (HnCommentRow *)user_data;
  HnNode *root_node = NULL;
  g_autoptr(GError) error = NULL;

  // if row is not available anymore then withdraw the result
  if (row == NULL) return;

  root_node = hn_client_get_comments_full_finish (HN_CLIENT (object), res, &error);
  if (error != NULL) {
    g_warning ("%s", error->message);
    return;
  }
  hn_comment_row_show_full (row, root_node);
}

static void
_show_comments_full (HnCommentPage *comments_page,
                     HnItem        *item,
                     HnCommentRow  *row,
                     gpointer       user_data)
{
  HnApplicationWindow *self = (HnApplicationWindow *)user_data;

  g_assert (HN_IS_APPLICATION_WINDOW (self));
  g_assert (HN_IS_COMMENT_PAGE (comments_page));
  g_assert (HN_IS_ITEM (item));
  g_assert (HN_IS_COMMENT_ROW (row));

  hn_comment_row_reveal (row);
  self->cancellable = g_cancellable_new ();
  hn_client_get_comments_full_async (self->client, item, self->cancellable, _show_comments_full_fetched, row);
  g_object_unref (self->cancellable);
}

static void
hn_application_window_init (HnApplicationWindow *self)
{
  HN_ENTRY;

  g_type_ensure (HDY_TYPE_CLAMP);
  g_type_ensure (HN_TYPE_ARTICLE_PAGE);
  g_type_ensure (HN_TYPE_STORY_LIST_BOX);
  g_type_ensure (HN_TYPE_COMMENT_PAGE);

  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("de.gunibert.Hackgregator");
  hdy_deck_set_transition_type (HDY_DECK (self->deck), HDY_DECK_TRANSITION_TYPE_OVER);

  self->client = hn_client_new ("https://hacker-news.firebaseio.com/v0");
  GPtrArray *endpoints = hn_client_get_available_endpoints (self->client);

  for (guint i = 0; i < endpoints->len; i++)
    {
      gchar *endpoint_id = g_ptr_array_index (endpoints, i);
      gchar *endpoint_name = hn_client_get_endpoint_name (self->client, endpoint_id);
      gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (self->sections), endpoint_id, endpoint_name);
    }

  self->active_section = g_ptr_array_index (endpoints, 0);

  g_ptr_array_free (endpoints, FALSE);

  self->item_list = g_list_store_new (HN_TYPE_ITEM);
  self->comment_list = g_list_store_new (HN_TYPE_ITEM);

  hn_application_window_setup_initial (self);
  gtk_combo_box_set_active (GTK_COMBO_BOX (self->sections), 0);

  WebKitWebView *webview = hn_article_page_get_webview (HN_ARTICLE_PAGE (self->article_page));
  g_signal_connect (webview,
                    "load-changed",
                    G_CALLBACK (hn_application_window_webview_load_changed),
                    self);

  hn_story_list_box_bind_model (HN_STORY_LIST_BOX (self->story_list_box), G_LIST_MODEL (self->item_list));
  g_signal_connect (self->story_list_box, "show_story", G_CALLBACK (hn_application_window_show_story), self);
  g_signal_connect (self->story_list_box, "show_comments", G_CALLBACK (_load_comments_page), self);
  hn_comment_page_bind_model (HN_COMMENT_PAGE (self->comment_page), G_LIST_MODEL (self->comment_list));
  g_signal_connect (self->comment_page, "show-full-comments", G_CALLBACK (_show_comments_full), self);

  HN_EXIT;
}

HnApplicationWindow *
hn_application_window_new (HnApplication *application)
{
  return g_object_new (HN_TYPE_APPLICATION_WINDOW,
                       "application", application,
                       NULL);
}

