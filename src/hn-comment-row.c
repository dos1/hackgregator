/* hn-comment-row.c
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "hn-comment-row"

#include "hn-comment-row.h"
#include "hn-utils.h"

struct _HnCommentRow
{
  GtkListBoxRow parent_instance;

  GtkWidget *sub_comments_revealer;
  GtkWidget *sub_comments_box;
  GtkWidget *comment_stack;
  GtkWidget *top_lbl;
  GtkWidget *author_lbl;
  GtkWidget *comment_count;

  HnItem *item;
  gboolean loaded;
};

G_DEFINE_TYPE (HnCommentRow, hn_comment_row, GTK_TYPE_LIST_BOX_ROW)

enum {
  PROP_0,
  PROP_ITEM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
hn_comment_row_new (HnItem *item)
{
  return g_object_new (HN_TYPE_COMMENT_ROW,
                       "item", item,
                       NULL);
}

static void
hn_comment_row_constructed (GObject *object)
{
  HnCommentRow *self = (HnCommentRow *)object;
  G_OBJECT_CLASS (hn_comment_row_parent_class)->constructed (object);

  gtk_label_set_text (GTK_LABEL (self->author_lbl), hn_item_get_author (self->item));
  gtk_label_set_markup (GTK_LABEL (self->top_lbl),
                        html_2_pango (hn_item_get_text (self->item)));

  GArray *kids = hn_item_get_children (self->item);
  g_autofree gchar *descendants = g_strdup_printf ("%d comments", kids->len);

  gtk_label_set_text (GTK_LABEL (self->comment_count), descendants);
}

static void
hn_comment_row_finalize (GObject *object)
{
  HnCommentRow *self = (HnCommentRow *)object;

  g_clear_pointer (&self->item, g_object_unref);

  G_OBJECT_CLASS (hn_comment_row_parent_class)->finalize (object);
}

static void
hn_comment_row_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  HnCommentRow *self = HN_COMMENT_ROW (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_value_set_object (value, self->item);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_comment_row_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  HnCommentRow *self = HN_COMMENT_ROW (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_clear_pointer (&self->item, g_object_unref);
      self->item = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_comment_row_class_init (HnCommentRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = hn_comment_row_constructed;
  object_class->finalize = hn_comment_row_finalize;
  object_class->get_property = hn_comment_row_get_property;
  object_class->set_property = hn_comment_row_set_property;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/Hackgregator/hn-comment-row.ui");
  gtk_widget_class_bind_template_child (widget_class, HnCommentRow, sub_comments_revealer);
  gtk_widget_class_bind_template_child (widget_class, HnCommentRow, sub_comments_box);
  gtk_widget_class_bind_template_child (widget_class, HnCommentRow, comment_stack);
  gtk_widget_class_bind_template_child (widget_class, HnCommentRow, top_lbl);
  gtk_widget_class_bind_template_child (widget_class, HnCommentRow, author_lbl);
  gtk_widget_class_bind_template_child (widget_class, HnCommentRow, comment_count);

  properties [PROP_ITEM] =
    g_param_spec_object ("item",
                         "Item",
                         "Item",
                         HN_TYPE_ITEM,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_ITEM,
                                   properties [PROP_ITEM]);
}

static void
hn_comment_row_init (HnCommentRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  self->loaded = FALSE;
}

static gboolean
_create_comment (HnNode    *node,
                 gpointer  user_data)
{
  HnCommentRow *self = HN_COMMENT_ROW (user_data);
  if (hn_node_depth (node) == 1) return FALSE;
  HnItem *item = node->data;
  if (hn_item_get_deleted (item)) return FALSE;
  g_debug ("%d %s", hn_node_depth (node), hn_item_get_text (item));

  gint margin_left = (hn_node_depth (node)-1) * 10;

  /* container */
  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);

  /* author */
  GtkWidget *author_lbl = gtk_label_new (hn_item_get_author (item));
  gtk_widget_set_margin_start (author_lbl, margin_left);
  gtk_label_set_xalign (GTK_LABEL (author_lbl), 0.0f);

  GtkStyleContext *context = gtk_widget_get_style_context (author_lbl);
  gtk_style_context_add_class (context, "comment-count");

  /* comment */
  GtkWidget *lbl = gtk_label_new (NULL);
  gtk_widget_set_margin_start (lbl, margin_left);
  gtk_label_set_markup (GTK_LABEL (lbl),
                        html_2_pango (hn_item_get_text (item)));
  gtk_label_set_xalign (GTK_LABEL (lbl), 0.0f);
  gtk_label_set_line_wrap (GTK_LABEL (lbl), TRUE);
  gtk_label_set_line_wrap_mode (GTK_LABEL (lbl), PANGO_WRAP_WORD);
  gtk_label_set_justify (GTK_LABEL (lbl), GTK_JUSTIFY_FILL);

  context = gtk_widget_get_style_context (lbl);
  gtk_style_context_add_class (context, "comment");

  gtk_box_pack_start (GTK_BOX (box), author_lbl, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (box), lbl, TRUE, TRUE, 0);

  gtk_widget_show_all (box);
  gtk_box_pack_start (GTK_BOX (self->sub_comments_box), box, FALSE, FALSE, 0);

  return FALSE;
}

void
hn_comment_row_show_full (HnCommentRow *self,
                          HnNode       *root_node)
{
  g_return_if_fail (HN_IS_COMMENT_ROW (self));

  hn_node_traverse_pre_order (root_node, G_TRAVERSE_ALL, _create_comment, self);

  gtk_stack_set_visible_child_name (GTK_STACK (self->comment_stack), "comments");
  self->loaded = TRUE;
}

void
hn_comment_row_reveal (HnCommentRow *self)
{
  g_return_if_fail (HN_IS_COMMENT_ROW (self));
  if (gtk_revealer_get_child_revealed (GTK_REVEALER (self->sub_comments_revealer)))
    {
      gtk_revealer_set_reveal_child (GTK_REVEALER (self->sub_comments_revealer), FALSE);

      return;
    }

  gtk_revealer_set_reveal_child (GTK_REVEALER (self->sub_comments_revealer), TRUE);
}

gboolean
hn_comment_row_is_loaded (HnCommentRow *self)
{
  g_return_val_if_fail (HN_IS_COMMENT_ROW (self), FALSE);

  return self->loaded;
}
