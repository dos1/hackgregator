#include "hn-story-list-box.h"
#include "hn-item.h"
#include "hn-debug.h"

struct _HnStoryListBox
{
  GtkBin parent_instance;

  GtkWidget *item_list_box;
  GtkWidget *item_list_box_sw;

  GListModel *model;
};

G_DEFINE_TYPE (HnStoryListBox, hn_story_list_box, GTK_TYPE_BIN)

enum {
  SHOW_STORY,
  SHOW_COMMENTS,
  N_SIGNALS
};

static guint signals[N_SIGNALS];

HnStoryListBox *
hn_story_list_box_new (void)
{
  return g_object_new (HN_TYPE_STORY_LIST_BOX, NULL);
}

static void
hn_story_list_box_finalize (GObject *object)
{
  HnStoryListBox *self = (HnStoryListBox *)object;

  g_clear_object (&self->model);

  G_OBJECT_CLASS (hn_story_list_box_parent_class)->finalize (object);
}

static void
on_item_list_box_row_activated (GtkListBox    *listbox,
                                GtkListBoxRow *row,
                                gpointer       user_data)
{
  HnStoryListBox *self = (HnStoryListBox *)user_data;

  HN_ENTRY;

  g_assert (HN_IS_STORY_LIST_BOX (self));

  gint idx = gtk_list_box_row_get_index (row);
  HnItem *item = g_list_model_get_item (G_LIST_MODEL (self->model), idx);
  g_signal_emit (self, signals[SHOW_STORY], 0, item);

  HN_EXIT;
}

static void
hn_story_list_box_class_init (HnStoryListBoxClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = hn_story_list_box_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/de/gunibert/Hackgregator/hn-story-list-box.ui");
  gtk_widget_class_bind_template_child (widget_class, HnStoryListBox, item_list_box);
  gtk_widget_class_bind_template_child (widget_class, HnStoryListBox, item_list_box_sw);
  gtk_widget_class_bind_template_callback (widget_class, on_item_list_box_row_activated);

  signals[SHOW_STORY] =
    g_signal_new ("show_story",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1, HN_TYPE_ITEM);

  signals [SHOW_COMMENTS] =
    g_signal_new ("show_comments",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  1, HN_TYPE_ITEM);
}

static void
hn_story_list_box_init (HnStoryListBox *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static HnItem *
hn_story_list_box_find_item (HnStoryListBox *self,
                             GtkButton      *btn)
{
  GtkWidget *parent = NULL;
  gint idx;

  g_assert (HN_IS_STORY_LIST_BOX (self));
  g_assert (GTK_IS_BUTTON (btn));

  parent = GTK_WIDGET (btn);

  while (!GTK_IS_LIST_BOX_ROW (parent))
    {
      parent = gtk_widget_get_parent (parent);

      if (parent == NULL)
          g_error ("This should never be happen");
    }

  idx = gtk_list_box_row_get_index (GTK_LIST_BOX_ROW (parent));
  gtk_list_box_select_row (GTK_LIST_BOX (self->item_list_box), GTK_LIST_BOX_ROW (parent));
  return g_list_model_get_item (self->model, idx);
}

static void
hn_story_list_box_load_comments (GtkButton *btn,
                                 gpointer   user_data)
{
  HnStoryListBox *self = (HnStoryListBox *)user_data;
  HnItem *item = NULL;

  item = hn_story_list_box_find_item (self, btn);
  g_signal_emit (self, signals[SHOW_COMMENTS], 0, item);
}

static GtkWidget *
hn_story_list_box_row_cb (gpointer item,
                          gpointer user_data)
{
  HnStoryListBox *self = (HnStoryListBox *) user_data;

  g_assert (HN_IS_STORY_LIST_BOX (self));
  g_assert (HN_IS_ITEM (item));

  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);
  gtk_widget_set_margin_top (box, 5);
  gtk_widget_set_margin_bottom (box, 5);
  gtk_widget_set_margin_start (box, 5);
  gtk_widget_set_margin_end (box, 5);

  g_autofree gchar *pos_str = g_strdup_printf ("%d.", hn_item_get_pos (item) + 1);
  GtkWidget *position_lbl = gtk_label_new (pos_str);
  gtk_label_set_width_chars (GTK_LABEL (position_lbl), 3);
  gtk_label_set_xalign (GTK_LABEL (position_lbl), 0.0f);
  gtk_box_pack_start (GTK_BOX (box), position_lbl, FALSE, FALSE, 5);

  GtkWidget *lbl = gtk_label_new (hn_item_get_title (item));
  gtk_label_set_line_wrap (GTK_LABEL (lbl), TRUE);
  gtk_label_set_xalign (GTK_LABEL (lbl), 0.0f);

  gtk_box_pack_start (GTK_BOX (box), lbl, TRUE, TRUE, 5);

  GtkWidget *counter_btn = gtk_button_new ();
  gtk_widget_set_can_focus (counter_btn, FALSE);
  gtk_button_set_relief (GTK_BUTTON (counter_btn), GTK_RELIEF_NONE);
  g_signal_connect (counter_btn, "clicked", G_CALLBACK (hn_story_list_box_load_comments), self);
  gtk_widget_set_valign (counter_btn, GTK_ALIGN_CENTER);
  gtk_widget_set_tooltip_text (counter_btn, "Show comments");

  g_autofree gchar *c = g_strdup_printf ("%d", hn_item_get_descendants (item));
  GtkWidget *counter = gtk_label_new (c);
  gtk_label_set_width_chars (GTK_LABEL (counter), 4);
  GtkStyleContext *context = gtk_widget_get_style_context (counter);
  gtk_style_context_add_class (context, "counter-label");
  gtk_widget_set_valign (counter, GTK_ALIGN_CENTER);

  gtk_container_add (GTK_CONTAINER (counter_btn), counter);
  gtk_box_pack_start (GTK_BOX (box), counter_btn, FALSE, FALSE, 5);

  gtk_widget_show_all (box);

  return box;
}

void
hn_story_list_box_bind_model (HnStoryListBox *self,
                              GListModel     *model)
{
  g_return_if_fail (HN_IS_STORY_LIST_BOX (self));
  g_return_if_fail (G_IS_LIST_MODEL (model));

  self->model = g_object_ref (model);
  gtk_list_box_bind_model (GTK_LIST_BOX (self->item_list_box),
                           model,
                           hn_story_list_box_row_cb,
                           self,
                           NULL);
}
