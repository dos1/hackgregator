/* hn-client.c
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */


/**
 * SECTION:hn-client
 * @Short_description: Fetch items from a HN API url
 * @Title: HnClient
 *
 * A GtkBuilder is an auxiliary object that reads textual descriptions
 * of a user interface and instantiates the described objects. To create
 * a GtkBuilder from a user interface description, call
 * gtk_builder_new_from_file(), gtk_builder_new_from_resource() or
 * gtk_builder_new_from_string().
 */

#define G_LOG_DOMAIN "hn-client"

#include "hn-client.h"
#include "hn-item-enums.h"
#include "hn-api.h"
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>
#include "hn-debug.h"

#include "hn-endpoints.h"

struct _HnClient
{
  GObject parent_instance;

  gchar *baseurl;
  SoupSession *session;
  HnApi *api;
};

G_DEFINE_TYPE (HnClient, hn_client, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_BASE_URL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * hn_client_new:
 * @baseurl: the api endpoint base
 *
 * Creates a #HnClient.
 *
 * Returns: a new #HnClient
 */
HnClient *
hn_client_new (gchar *baseurl)
{
  return g_object_new (HN_TYPE_CLIENT,
                       "base-url", baseurl,
                       NULL);
}


static void
hn_client_constructed (GObject *object)
{
  HnClient *self = HN_CLIENT (object);
  G_OBJECT_CLASS (hn_client_parent_class)->constructed (object);

  self->api = hn_api_new (self->baseurl);

  hn_api_add_endpoint (self->api, HN_API_ENDPOINT (hn_top_stories_new ()));
  hn_api_add_endpoint (self->api, HN_API_ENDPOINT (hn_new_stories_new ()));
  hn_api_add_endpoint (self->api, HN_API_ENDPOINT (hn_ask_stories_new ()));
  hn_api_add_endpoint (self->api, HN_API_ENDPOINT (hn_show_stories_new ()));
}

static void
hn_client_finalize (GObject *object)
{
  HnClient *self = (HnClient *)object;

  g_clear_pointer (&self->baseurl, g_free);
  g_clear_pointer (&self->api, g_object_unref);

  G_OBJECT_CLASS (hn_client_parent_class)->finalize (object);
}

static void
hn_client_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  HnClient *self = HN_CLIENT (object);

  switch (prop_id)
    {
    case PROP_BASE_URL:
        g_value_set_string (value, self->baseurl);
        break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_client_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  HnClient *self = HN_CLIENT (object);

  switch (prop_id)
    {
    case PROP_BASE_URL:
      g_clear_pointer (&self->baseurl, g_free);
      self->baseurl = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_client_class_init (HnClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = hn_client_finalize;
  object_class->constructed = hn_client_constructed;
  object_class->get_property = hn_client_get_property;
  object_class->set_property = hn_client_set_property;

  properties [PROP_BASE_URL] =
    g_param_spec_string ("base-url",
                         "BaseUrl",
                         "BaseUrl",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_BASE_URL,
                                   properties [PROP_BASE_URL]);
}

static void
hn_client_init (HnClient *self)
{
  self->session = soup_session_new ();
}

static gint
_stories_sort_func (gconstpointer a,
                    gconstpointer b)
{
  HnItem *item_a = *(HnItem **) a;
  HnItem *item_b = *(HnItem **) b;

  return hn_item_get_pos (item_a) - hn_item_get_pos (item_b);
}

/**
 * hn_client_get_base_url:
 * @self: a #HnClient
 *
 * Return this clients base url
 *
 * Returns: the base url this client is configured
 */
gchar *
hn_client_get_base_url (HnClient *self)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);
  g_return_val_if_fail (self->baseurl != NULL, NULL);

  return self->baseurl;
}


typedef struct {
  HnClient *client;
  GPtrArray *items;
} HnClientItemData;

static void
_iterate_kids (JsonArray *array,
               guint      index_,
               JsonNode  *element_node,
               gpointer   user_data)
{
  HnItem *item = HN_ITEM (user_data);

  gint64 kid = json_node_get_int (element_node);
  hn_item_add_child (item, kid);
}

/**
 * hn_client_get_item:
 * @self: a #HnClient
 * @id: the identifier for the #HnItem
 *
 * Fetches a item synchronously from the api with the corresponding identifier.
 * This blocks until constructed the item.
 *
 * Returns: (transfer full): returns a #HnItem
 */
HnItem *
hn_client_get_item (HnClient *self,
                    guint     id)
{

  g_autoptr(SoupMessage) msg = NULL;
  g_autoptr(JsonParser) parser = NULL;
  HnItem *item = NULL;

  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);

  HN_ENTRY;

  item = hn_item_new ();

  parser = json_parser_new ();

  msg = hn_api_get_item (self->baseurl, id);
  soup_session_send_message (self->session, msg);

  const gchar *data = msg->response_body->data;
  if (data == NULL) return NULL;
  json_parser_load_from_data (parser, data, -1, NULL);

  JsonNode *root = json_parser_get_root (parser);
  JsonObject *object = json_node_get_object (root);

  if (json_object_has_member (object, "type"))
    {
      const gchar *itemtype = json_object_get_string_member (object, "type");
      GEnumClass *enum_class = g_type_class_ref (HN_TYPE_ITEM_TYPE);
      GEnumValue *enum_value = g_enum_get_value_by_nick (enum_class, itemtype);
      hn_item_set_item_type (item, enum_value->value);
    }

  if (json_object_has_member (object, "id"))
    {
      gint64 id = json_object_get_int_member (object, "id");
      hn_item_set_id (item, id);
    }

  if (json_object_has_member (object, "title"))
    {
      const gchar *title = json_object_get_string_member (object, "title");
      hn_item_set_title (item, title);
    }

  if (json_object_has_member (object, "text"))
    {
      const gchar *text = json_object_get_string_member (object, "text");
      hn_item_set_text (item, text);
    }

  if (json_object_has_member (object, "url"))
    {
      const gchar *url = json_object_get_string_member (object, "url");
      hn_item_set_url (item, url);
    }

  if (json_object_has_member (object, "by"))
    {
      const gchar *author = json_object_get_string_member (object, "by");
      hn_item_set_author (item, author);
    }

  if (json_object_has_member (object, "time"))
    {
      gint64 time = json_object_get_int_member (object, "time");
      hn_item_set_creation_time (item, time);
    }

  if (json_object_has_member (object, "score"))
    {
      guint score = json_object_get_int_member (object, "score");
      hn_item_set_score (item, score);
    }

  if (json_object_has_member (object, "kids"))
    {
      JsonArray *kids = json_object_get_array_member (object, "kids");
      json_array_foreach_element (kids, _iterate_kids, item);
    }

  if (json_object_has_member (object, "descendants"))
    {
      gint descendants = json_object_get_int_member (object, "descendants");
      hn_item_set_descendants (item, descendants);
    }

  if (json_object_has_member (object, "deleted"))
    {
      gboolean deleted = json_object_get_boolean_member (object, "deleted");
      hn_item_set_deleted (item, deleted);
    }

  HN_RETURN (item);
}

typedef struct {
  guint id;
  guint pos;
} HnClientThreadData;

static void
hn_client_get_items_thread (gpointer data,
                            gpointer user_data)
{
  HnClientThreadData *tdata = data;
  HnClientItemData *itemdata = user_data;

  HN_ENTRY;

  HnItem *item = hn_client_get_item (itemdata->client, tdata->id);
  if (item == NULL)
    {
      g_free (tdata);
      return;
    }
  hn_item_set_pos (item, tdata->pos);
  g_ptr_array_add (itemdata->items, item);
  g_free (tdata);

  HN_EXIT;
}

typedef struct {
  gint limit;
  const gchar *endpoint_id;
} HnStoriesData;

static void
hn_client_get_stories_worker (GTask        *task,
                              gpointer      source_object,
                              gpointer      task_data,
                              GCancellable *cancellable)
{
  HnClient *self = HN_CLIENT (source_object);
  g_autoptr(SoupMessage) msg = NULL;
  g_autoptr(JsonParser) parser = NULL;
  GPtrArray *ret = NULL;

  JsonNode *root = NULL;
  JsonArray *array = NULL;
  const gchar *data = NULL;
  HnClientItemData *itemdata = NULL;
  GThreadPool *pool = NULL;

  g_assert (G_IS_TASK (task));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  HN_ENTRY;

  HnStoriesData *stories_data = task_data;

  if (stories_data->limit == -1)
    stories_data->limit = 30;

  ret = g_ptr_array_new_full (stories_data->limit, g_object_unref);
  msg = hn_api_get_endpoint (self->api, stories_data->endpoint_id);
  soup_session_send_message (self->session, msg);

  parser = json_parser_new ();
  data = msg->response_body->data;
  json_parser_load_from_data (parser, data, -1, NULL);

  root = json_parser_get_root (parser);
  array = json_node_get_array (root);

  itemdata = g_new0 (HnClientItemData, 1);
  itemdata->client = self;
  itemdata->items = ret;
  pool = g_thread_pool_new (hn_client_get_items_thread, itemdata, -1, FALSE, NULL);

  for (gint i = 0; i < stories_data->limit; i++)
    {
      JsonNode *node = json_array_get_element (array, i);

      gint id = json_node_get_int (node);
      HnClientThreadData *tdata = g_new0 (HnClientThreadData, 1);
      tdata->id = id;
      tdata->pos = i;
      g_thread_pool_push (pool, tdata, NULL);
    }

  g_thread_pool_free (pool, FALSE, TRUE);

  g_free (itemdata);
  g_ptr_array_sort (ret, _stories_sort_func);

  g_task_return_pointer (task, ret, NULL);

  HN_EXIT;
}

/**
 * hn_client_get_stories_async:
 * @self: a #HnClient
 * @limit: the amount of stories to fetch, or -1 for default
 * @endpoint: the type of stories to fetch
 * @cancellable: a #GCancellable object or %NULL
 * @callback: a callback to call with the result
 * @user_data: data for callback or %NULL
 *
 *
 */
void
hn_client_get_stories_async (HnClient            *self,
                             gint                 limit,
                             const gchar         *endpoint_id,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  HnStoriesData *data = NULL;

  g_return_if_fail (HN_IS_CLIENT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  HN_ENTRY;

  data = g_new0 (HnStoriesData, 1);
  data->limit = limit;
  data->endpoint_id = endpoint_id;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, hn_client_get_stories_async);
  g_task_set_task_data (task, data, g_free);
  g_task_run_in_thread (task, hn_client_get_stories_worker);

  HN_EXIT;
}

/**
 * hn_client_get_stories_finish:
 * @self: an #HnClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns: (transfer full) (element-type HnItem): an array of Stories for the specified endpoint
 */
GPtrArray *
hn_client_get_stories_finish (HnClient *self,
                              GAsyncResult *result,
                              GError **error)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}

/**
 * hn_client_get_stories:
 * @self: a #HnClient
 * @limit: the limit how many items should be received up to 200 or -1 for default
 * @endpoint: the endpoint (stories, ask, show, ...)
 *
 * Returns: (transfer full) (element-type HnItem): an array of Stories for the
 * specified endpoint
 */
GPtrArray *
hn_client_get_stories (HnClient      *self,
                       gint           limit,
                       gchar         *endpoint_id)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);

  g_autoptr (GTask) task = NULL;
  HnStoriesData *data = NULL;

  data = g_new0 (HnStoriesData, 1);
  data->limit = limit;
  data->endpoint_id = endpoint_id;

  task = g_task_new (self, NULL, NULL, NULL);
  g_task_set_source_tag (task, hn_client_get_stories);
  g_task_set_task_data (task, data, g_free);
  g_task_run_in_thread_sync (task, hn_client_get_stories_worker);

  return hn_client_get_stories_finish (self, G_ASYNC_RESULT (task), NULL);
}

static void
hn_client_get_comments_worker (GTask        *task,
                               gpointer      source_object,
                               gpointer      task_data,
                               GCancellable *cancellable)
{
  HnClient *self = HN_CLIENT (source_object);

  GThreadPool *pool = NULL;
  HnClientItemData *itemdata = NULL;
  GPtrArray *ret = NULL;
  HnItem *parent = HN_ITEM (task_data);
  GArray *kids = hn_item_get_children (parent);
  ret = g_ptr_array_new_full (kids->len, NULL);

  itemdata = g_new0 (HnClientItemData, 1);
  itemdata->client = self;
  itemdata->items = ret;
  pool = g_thread_pool_new (hn_client_get_items_thread, itemdata, -1, FALSE, NULL);

  for (guint i = 0; i < kids->len; i++)
    {

      HnClientThreadData *tdata = g_new0 (HnClientThreadData, 1);
      tdata->id = g_array_index (kids, gint64, i);
      tdata->pos = i;
      g_thread_pool_push (pool, tdata, NULL);
    }

  g_thread_pool_free (pool, FALSE, TRUE);
  g_ptr_array_sort (ret, _stories_sort_func);

  g_free (itemdata);

  g_task_return_pointer (task, ret, NULL);
}

/**
 * hn_client_get_comments:
 * @self: an #HnClient
 * @parent: the toplevel #HnItem to fetch from
 * @error: a location for a #GError, or %NULL
 *
 * Test
 *
 * Returns: (transfer full) (element-type HnItem):
 */
GPtrArray *
hn_client_get_comments (HnClient  *self,
                        HnItem    *parent,
                        GError   **error)
{
  g_autoptr(GTask) task = NULL;

  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);

  task = g_task_new (self, NULL, NULL, NULL);
  g_task_set_source_tag (task, hn_client_get_comments);
  g_task_set_task_data (task, parent, NULL);
  g_task_run_in_thread_sync (task, hn_client_get_comments_worker);

  return g_task_propagate_pointer (task, error);
}

/**
 * hn_client_get_comments_async:
 * @self: an #HnClient
 * @parent: the toplevel #HnItem to fetch from
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 */
void
hn_client_get_comments_async (HnClient            *self,
                              HnItem              *parent,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (HN_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, hn_client_get_comments_async);
  g_task_set_task_data (task, parent, NULL);
  g_task_run_in_thread (task, hn_client_get_comments_worker);
}

/**
 * hn_client_get_comments_finish:
 * @self: an #HnClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns: (transfer full) (element-type HnItem):
 */
GPtrArray *
hn_client_get_comments_finish (HnClient *self,
                               GAsyncResult *result,
                               GError **error)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

typedef struct
{
  gint64 id;
  HnNode *node;
} CommentsData;

static void
_get_comments_deep_thread (gpointer data,
                           gpointer user_data)
{
  HnClientItemData *clientdata = user_data;
  CommentsData *cdata = data;
  HnItem *item = hn_client_get_item (clientdata->client, cdata->id);
  GArray *children = hn_item_get_children (item);

  cdata->node->data = item;
  for (guint i = 0; i < children->len; i++)
    {
      HnNode *node = hn_node_new (NULL);
      gint64 id = g_array_index (children, gint64, i);
      g_debug ("fetching now id %ld", id);

      hn_node_append (cdata->node, node);

      CommentsData *data = g_new0 (CommentsData, 1);
      data->id = id;
      data->node = node;

      _get_comments_deep_thread (data, user_data);
    }

  g_free (cdata);
}

static void
hn_client_get_comments_full_worker (GTask        *task,
                                    gpointer      source_object,
                                    gpointer      task_data,
                                    GCancellable *cancellable)
{
  HnClient *self = HN_CLIENT (source_object);
  HnNode *root_node = (HnNode *)task_data;
  HnItem *root = root_node->data;
  GArray *children = hn_item_get_children (root);
  GThreadPool *pool = NULL;

  HnClientItemData *clientdata = g_new0 (HnClientItemData, 1);
  clientdata->client = self;

  pool = g_thread_pool_new (_get_comments_deep_thread, clientdata, -1, FALSE, NULL);
  for (guint i = 0; i < children->len; i++)
    {
      HnNode *node = hn_node_new (NULL);
      gint64 id = g_array_index (children, gint64, i);

      hn_node_append (root_node, node);

      CommentsData *data = g_new0 (CommentsData, 1);
      data->id = id;
      data->node = node;

      g_thread_pool_push (pool, data, NULL);
    }
  g_thread_pool_free (pool, FALSE, TRUE);

  g_free (clientdata);

  g_task_return_pointer (task, root_node, NULL);
}

/**
 * hn_client_get_comments_full_async:
 * @self: an #HnClient
 * @root: the toplevel #HnItem to fetch from
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 */
void
hn_client_get_comments_full_async (HnClient            *self,
                                   HnItem              *root,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (HN_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, hn_client_get_comments_full_async);

  HnNode *root_node = hn_node_new (root);
  g_task_set_task_data (task, root_node, NULL);
  g_task_run_in_thread (task, hn_client_get_comments_full_worker);
}

/**
 * hn_client_get_comments_full_finish:
 * @self: an #HnClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Returns: (transfer full):
 */
HnNode *
hn_client_get_comments_full_finish (HnClient *self,
                                    GAsyncResult *result,
                                    GError **error)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

GPtrArray *
hn_client_get_available_endpoints (HnClient *self)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);

  return hn_api_get_available_endpoints (self->api);
}

gchar *
hn_client_get_endpoint_name (HnClient *self,
                             gchar    *endpoint_id)
{
  g_return_val_if_fail (HN_IS_CLIENT (self), NULL);

  return hn_api_get_endpoint_name (self->api, endpoint_id);
}
