/* hn-log.c
 *
 * Copyright 2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "hn-debug.h"

static int log_verbosity;

static const gchar *
hn_log_level_str_with_color (GLogLevelFlags log_level)
{
  switch (((gulong)log_level & G_LOG_LEVEL_MASK))
    {
    case G_LOG_LEVEL_ERROR:    return "   \033[1;31mERROR\033[0m";
    case G_LOG_LEVEL_CRITICAL: return "\033[1;35mCRITICAL\033[0m";
    case G_LOG_LEVEL_WARNING:  return " \033[1;33mWARNING\033[0m";
    case G_LOG_LEVEL_MESSAGE:  return " \033[1;32mMESSAGE\033[0m";
    case G_LOG_LEVEL_INFO:     return "    \033[1;32mINFO\033[0m";
    case G_LOG_LEVEL_DEBUG:    return "   \033[1;32mDEBUG\033[0m";
    case HN_LOG_LEVEL_TRACE:   return "   \033[1;36mTRACE\033[0m";

    default:
      return " UNKNOWN";
    }
}

static void
hn_log_handler (const gchar    *log_domain,
                GLogLevelFlags  log_level,
                const gchar    *message,
                gpointer        user_data)
{
  gint64 now;
  gchar *buffer;
  gchar ftime[32];
  struct tm tt;
  time_t t;

  switch ((int)log_level)
    {
    case G_LOG_LEVEL_MESSAGE:
      if (log_verbosity < 1)
        return;
      break;

    case G_LOG_LEVEL_INFO:
      if (log_verbosity < 2)
        return;
      break;

    case G_LOG_LEVEL_DEBUG:
      if (log_verbosity < 3)
        return;
      break;

    case HN_LOG_LEVEL_TRACE:
      if (log_verbosity < 4)
        return;
      break;

    default:
      break;
    }

  now = g_get_real_time ();
  t = now / G_USEC_PER_SEC;
  tt = *localtime (&t);
  strftime (ftime, sizeof (ftime), "%H:%M:%S", &tt);
  const gchar *level;
  level = hn_log_level_str_with_color (log_level);

  buffer = g_strdup_printf ("%s.%04d  %40s: %s: %s\n",
                            ftime,
                            (gint)((now % G_USEC_PER_SEC) / 100L),
                            log_domain,
                            level,
                            message);
  g_print ("%s", buffer);

  g_free (buffer);
}

void
hn_log_init ()
{
  static gsize initialized = FALSE;

  if (g_once_init_enter (&initialized))
    {
      g_log_set_default_handler (hn_log_handler, NULL);
      g_once_init_leave (&initialized, TRUE);
    }

}

void
hn_log_increase_verbosity (void)
{
  log_verbosity++;
}

gint
hn_log_get_verbosity (void)
{
  return log_verbosity;
}

void
hn_log_set_verbosity (gint level)
{
  log_verbosity = level;
}
