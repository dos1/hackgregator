/* main.c
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>
#include "hn-application.h"
#include "hn-log.h"

static gboolean
verbose_cb (const gchar  *option_name,
            const gchar  *value,
            gpointer      data,
            GError      **error)
{
  hn_log_increase_verbosity ();
  return TRUE;
}

gint
main (gint   argc,
      gchar *argv[])
{
  hn_log_init ();

  static const GOptionEntry options[] = {
      { "verbose", 'v', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, verbose_cb, "Increase log verbosity" },
      NULL
  };

  HnApplication *app = hn_application_new ();

  g_application_add_main_option_entries (G_APPLICATION (app), options);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
