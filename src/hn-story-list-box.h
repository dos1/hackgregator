#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define HN_TYPE_STORY_LIST_BOX (hn_story_list_box_get_type())

G_DECLARE_FINAL_TYPE (HnStoryListBox, hn_story_list_box, HN, STORY_LIST_BOX, GtkBin)

HnStoryListBox *hn_story_list_box_new          (void);
void            hn_story_list_box_bind_model   (HnStoryListBox *self,
                                                GListModel     *model);
G_END_DECLS
