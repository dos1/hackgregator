/* hn-api.h
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */


#pragma once

#include <glib-object.h>
#include <libsoup/soup.h>
#include "hn-api-endpoint.h"

G_BEGIN_DECLS

#define HN_TYPE_API (hn_api_get_type())

G_DECLARE_FINAL_TYPE (HnApi, hn_api, HN, API, GObject)

HnApi       *hn_api_new                     (gchar         *baseurl);
SoupMessage *hn_api_get_item                (gchar         *baseurl,
                                             guint          identifier);
SoupMessage *hn_api_get_endpoint            (HnApi         *self,
                                             const gchar   *endpoint_id);
void         hn_api_add_endpoint            (HnApi         *self,
                                             HnApiEndpoint *endpoint);
GPtrArray   *hn_api_get_available_endpoints (HnApi         *self);
gchar       *hn_api_get_endpoint_name       (HnApi         *self,
                                             gchar         *endpoint_id);

G_END_DECLS
