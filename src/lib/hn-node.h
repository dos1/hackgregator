/* hn-node.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define HN_TYPE_NODE (hn_node_get_type ())

typedef struct _HnNode HnNode;

struct _HnNode
{
  gpointer data;
  HnNode *next;
  HnNode *prev;
  HnNode *parent;
  HnNode *children;

  /*< private >*/
  guint ref_count;
};

typedef gboolean (*HnNodeTraverseFunc) (HnNode   *node,
                                        gpointer  data);

GType     hn_node_get_type           (void) G_GNUC_CONST;
HnNode   *hn_node_new                (gpointer            data);
HnNode   *hn_node_copy               (HnNode             *self);
HnNode   *hn_node_ref                (HnNode             *self);
void      hn_node_unref              (HnNode             *self);
HnNode   *hn_node_append             (HnNode             *parent,
                                      HnNode             *child);
gboolean  hn_node_traverse_pre_order (HnNode             *node,
                                      GTraverseFlags      flags,
                                      HnNodeTraverseFunc  func,
                                      gpointer            data);
guint     hn_node_depth              (HnNode             *node);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (HnNode, hn_node_unref)

G_END_DECLS
