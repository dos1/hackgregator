/* hackernews-version.h.in
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef HACKERNEWS_VERSION_H
#define HACKERNEWS_VERSION_H

#if !defined(HACKERNEWS_INSIDE) && !defined(HACKERNEWS_COMPILATION)
# error "Only <hackernews-glib.h> can be included directly."
#endif

/**
 * SECTION:hn-version
 * @short_description: hackernews version checking
 *
 * hackernews provides macros to check the version of the library
 * at compile-time
 */

/**
 * HACKERNEWS_MAJOR_VERSION:
 *
 * hackernews major version component (e.g. 1 if %HACKERNEWS_VERSION is 1.2.3)
 */
#define HACKERNEWS_MAJOR_VERSION (@MAJOR_VERSION@)

/**
 * HACKERNEWS_MINOR_VERSION:
 *
 * hackernews minor version component (e.g. 2 if %HACKERNEWS_VERSION is 1.2.3)
 */
#define HACKERNEWS_MINOR_VERSION (@MINOR_VERSION@)

/**
 * HACKERNEWS_MICRO_VERSION:
 *
 * hackernews micro version component (e.g. 3 if %HACKERNEWS_VERSION is 1.2.3)
 */
#define HACKERNEWS_MICRO_VERSION (@MICRO_VERSION@)

/**
 * HACKERNEWS_VERSION
 *
 * hackernews version.
 */
#define HACKERNEWS_VERSION (@VERSION@)

/**
 * HACKERNEWS_VERSION_S:
 *
 * hackernews version, encoded as a string, useful for printing and
 * concatenation.
 */
#define HACKERNEWS_VERSION_S "@VERSION@"

#define HACKERNEWS_ENCODE_VERSION(major,minor,micro) \
        ((major) << 24 | (minor) << 16 | (micro) << 8)

/**
 * HACKERNEWS_VERSION_HEX:
 *
 * hackernews version, encoded as an hexadecimal number, useful for
 * integer comparisons.
 */
#define HACKERNEWS_VERSION_HEX \
        (HACKERNEWS_ENCODE_VERSION (HACKERNEWS_MAJOR_VERSION, HACKERNEWS_MINOR_VERSION, HACKERNEWS_MICRO_VERSION))

/**
 * HACKERNEWS_CHECK_VERSION:
 * @major: required major version
 * @minor: required minor version
 * @micro: required micro version
 *
 * Compile-time version checking. Evaluates to %TRUE if the version
 * of hackernews is greater than the required one.
 */
#define HACKERNEWS_CHECK_VERSION(major,minor,micro)   \
        (HACKERNEWS_MAJOR_VERSION > (major) || \
         (HACKERNEWS_MAJOR_VERSION == (major) && HACKERNEWS_MINOR_VERSION > (minor)) || \
         (HACKERNEWS_MAJOR_VERSION == (major) && HACKERNEWS_MINOR_VERSION == (minor) && \
          HACKERNEWS_MICRO_VERSION >= (micro)))

#endif /* HACKERNEWS_VERSION_H */
