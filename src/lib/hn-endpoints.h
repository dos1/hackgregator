/* hn-endpoints.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define HN_TYPE_TOP_STORIES (hn_top_stories_get_type())
#define HN_TYPE_NEW_STORIES (hn_new_stories_get_type())
#define HN_TYPE_ASK_STORIES (hn_ask_stories_get_type())
#define HN_TYPE_SHOW_STORIES (hn_show_stories_get_type())

G_DECLARE_FINAL_TYPE (HnTopStories, hn_top_stories, HN, TOP_STORIES, GObject)
G_DECLARE_FINAL_TYPE (HnNewStories, hn_new_stories, HN, NEW_STORIES, GObject)
G_DECLARE_FINAL_TYPE (HnAskStories, hn_ask_stories, HN, ASK_STORIES, GObject)
G_DECLARE_FINAL_TYPE (HnShowStories, hn_show_stories, HN, SHOW_STORIES, GObject)

HnTopStories  *hn_top_stories_new  (void);
HnNewStories  *hn_new_stories_new  (void);
HnAskStories  *hn_ask_stories_new  (void);
HnShowStories *hn_show_stories_new (void);

G_END_DECLS

