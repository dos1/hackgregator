/* hn-item.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define HN_TYPE_ITEM (hn_item_get_type())

G_DECLARE_FINAL_TYPE (HnItem, hn_item, HN, ITEM, GObject)

/**
 * HnItemType:
 * @HN_ITEM_TYPE_STORY: item is a story
 * @HN_ITEM_TYPE_JOB: item is a job offer
 * @HN_ITEM_TYPE_COMMENT: item is a comment
 * @HN_ITEM_TYPE_POLL: item is a poll
 * @HN_ITEM_TYPE_POLLOPT: item is a poll option
 */
typedef enum {
  HN_ITEM_TYPE_STORY,
  HN_ITEM_TYPE_JOB,
  HN_ITEM_TYPE_COMMENT,
  HN_ITEM_TYPE_POLL,
  HN_ITEM_TYPE_POLLOPT,
} HnItemType;

HnItem    *hn_item_new               (void);
gint64     hn_item_get_id            (HnItem      *self);
void       hn_item_set_id            (HnItem      *self,
                                      gint64       id);
gchar     *hn_item_get_title         (HnItem      *self);
void       hn_item_set_title         (HnItem      *self,
                                      const gchar *title);
gchar     *hn_item_get_text          (HnItem      *self);
void       hn_item_set_text          (HnItem      *self,
                                      const gchar *text);
gchar     *hn_item_get_url           (HnItem      *self);
void       hn_item_set_url           (HnItem      *self,
                                      const gchar *url);
gchar     *hn_item_get_author        (HnItem      *self);
void       hn_item_set_author        (HnItem      *self,
                                      const gchar *author);
GDateTime *hn_item_get_creation_time (HnItem      *self);
void       hn_item_set_creation_time (HnItem      *self,
                                      gint64       ct);
guint      hn_item_get_score         (HnItem      *self);
void       hn_item_set_score         (HnItem      *self,
                                      guint        score);
guint      hn_item_get_pos           (HnItem      *self);
void       hn_item_set_pos           (HnItem      *self,
                                      guint        pos);
GArray    *hn_item_get_children      (HnItem      *self);
void       hn_item_add_child         (HnItem      *self,
                                      gint64       child_id);
gint       hn_item_get_descendants   (HnItem      *self);
void       hn_item_set_descendants   (HnItem      *self,
                                      gint         descendants);
gboolean   hn_item_get_deleted       (HnItem      *self);
void       hn_item_set_deleted       (HnItem      *self,
                                      gboolean     deleted);
HnItemType hn_item_get_item_type     (HnItem      *self);
void       hn_item_set_item_type     (HnItem      *self,
                                      HnItemType   type);
G_END_DECLS
