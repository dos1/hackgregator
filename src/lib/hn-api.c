/* hn-api.c
 *
 * Copyright 2019-2020 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:hn-api
 * @Short_description: the encapsulated api representation
 * @Title: HnApi
 *
 */

#define G_LOG_DOMAIN "hn-api"

#include "hn-api.h"

struct _HnApi
{
  GObject parent_instance;

  gchar *baseurl;
  GHashTable *endpoints;
};

G_DEFINE_TYPE (HnApi, hn_api, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_BASE_URL,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

HnApi *
hn_api_new (gchar *baseurl)
{
  return g_object_new (HN_TYPE_API, "base-url", baseurl, NULL);
}

static void
hn_api_finalize (GObject *object)
{
  HnApi *self = (HnApi *)object;

  g_clear_pointer (&self->baseurl, g_free);
  g_hash_table_unref (self->endpoints);

  G_OBJECT_CLASS (hn_api_parent_class)->finalize (object);
}

static void
hn_api_get_property (GObject    *object,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  HnApi *self = HN_API (object);

  switch (prop_id)
    {
    case PROP_BASE_URL:
        g_value_set_string (value, self->baseurl);
        break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_api_set_property (GObject      *object,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  HnApi *self = HN_API (object);

  switch (prop_id)
    {
    case PROP_BASE_URL:
      g_clear_pointer (&self->baseurl, g_free);
      self->baseurl = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_api_class_init (HnApiClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = hn_api_finalize;
  object_class->get_property = hn_api_get_property;
  object_class->set_property = hn_api_set_property;

  properties [PROP_BASE_URL] =
    g_param_spec_string ("base-url",
                         "BaseUrl",
                         "BaseUrl",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_BASE_URL,
                                   properties [PROP_BASE_URL]);
}

static void
hn_api_init (HnApi *self)
{
  self->endpoints = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_object_unref);
}

/**
 * hn_api_get_item:
 * @baseurl: the server URI
 * @identifier: the ID of the item
 *
 * Returns: (transfer full): a #SoupMessage to request a #HnItem
 */
SoupMessage *
hn_api_get_item (gchar *baseurl, guint identifier)
{
  g_autofree gchar *url;
  SoupMessage *msg = NULL;

  url = g_strdup_printf ("%s/item/%d.json", baseurl, identifier);
  msg = soup_message_new (SOUP_METHOD_GET, url);

  return msg;
}

/**
 * hn_api_get_endpoint:
 * @self: a #HnApi
 * @endpoint_id: the identifier of the endpoint
 *
 * Returns: (transfer full): returns a #SoupMessage to request endpoint
 */
SoupMessage *
hn_api_get_endpoint (HnApi       *self,
                     const gchar *endpoint_id)
{
  g_autofree gchar *url = NULL;
  SoupMessage *msg = NULL;

  HnApiEndpoint *endpoint = g_hash_table_lookup (self->endpoints, endpoint_id);

  url = g_strdup_printf (hn_api_endpoint_get_uri (endpoint), self->baseurl);

  msg = soup_message_new (SOUP_METHOD_GET, url);

  return msg;
}

void
hn_api_add_endpoint (HnApi         *self,
                     HnApiEndpoint *endpoint)
{
  g_hash_table_insert (self->endpoints, hn_api_endpoint_get_identifier (endpoint), endpoint);
}

/**
 * hn_api_get_available_endpoints:
 * @self: a #HnApi
 *
 * All available endpoint identifier currently usable. This creates the container and all
 * identifiers on the heap. The caller has to free this after usage.
 *
 * Returns: (transfer container) (element-type gchar): a newly allocated #GPtrArray with all identifier of the available endpoints
 */
GPtrArray *
hn_api_get_available_endpoints (HnApi *self)
{
  GPtrArray *ret = g_ptr_array_new_with_free_func (g_free);
  GList *values = g_hash_table_get_values (self->endpoints);

  for (GList *cur = values; cur; cur = g_list_next (cur))
    {
      HnApiEndpoint *endpoint = cur->data;
      g_ptr_array_add (ret, hn_api_endpoint_get_identifier (endpoint));
    }

  g_list_free (values);
  return ret;
}

/**
 * hn_api_get_endpoint_name:
 * @self: a #HnApi
 * @endpoint_id: the identifier of that endpoint
 *
 *
 * Returns: a newly allocated human readable representation of the endpoint
 */
gchar *
hn_api_get_endpoint_name (HnApi *self,
                          gchar *endpoint_id)
{
  HnApiEndpoint *endpoint = NULL;

  endpoint = g_hash_table_lookup (self->endpoints, endpoint_id);

  return g_strdup (hn_api_endpoint_get_name (endpoint));
}
